FROM golang:1.14.3 AS build-service

ADD . /modelconverter

WORKDIR /modelconverter
ENV GOPATH=/modelconverter

RUN go build -o /tmp/modelconverter ./src/modelconverter/main.go


FROM stephenradachy/usd-docker:19.11 AS converter

ARG UFG_RELEASE="3bf441e0eb5b6cfbe487bbf1e2b42b7447c43d02"
ARG UFG_REPO="/UFG"
ARG UFG_INSTALL="/usr/local/UFG"
ENV USD_DIR="/usr/local/USD"
ENV LD_LIBRARY_PATH="${USD_DIR}/lib:${UFG_REPO}/lib"
ENV PATH="${PATH}:${UFG_INSTALL}/bin"
ENV PYTHONPATH="${PYTHONPATH}:${UFG_INSTALL}/python" 

RUN git init "${UFG_REPO}" && \
    cd "${UFG_REPO}" && \
    git remote add origin https://github.com/google/usd_from_gltf.git && \
    git fetch --depth 1 origin "${UFG_RELEASE}" && \
    git checkout FETCH_HEAD
RUN python "${UFG_REPO}/tools/ufginstall/ufginstall.py" -v "${UFG_INSTALL}" "${USD_DIR}" && \
    cp -r "${UFG_REPO}/tools/ufgbatch" "${UFG_INSTALL}/python" &&\
    rm -rf "${UFG_REPO}" "${UFG_INSTALL}/build" "${UFG_INSTALL}/src"

COPY --from=build-service /tmp/modelconverter /usr/local/bin/modelconverter

RUN chmod +x /usr/local/bin/modelconverter

EXPOSE 1611
CMD ["/usr/local/bin/modelconverter"]
