PROJECT_NAME=modelconverter
CURRENT_DIR?=$(shell pwd)
SERVICE_PORT=1611

up:
	docker build -t $(PROJECT_NAME) .
	docker run -p $(SERVICE_PORT):$(SERVICE_PORT) --env-file=.env $(PROJECT_NAME) 

protoc:
	protoc -I proto/${PROJECT_NAME}/ proto/${PROJECT_NAME}/${PROJECT_NAME}.proto --go_out=plugins=grpc:src/$(PROJECT_NAME)/proto/${PROJECT_NAME}/

lint:
	@docker run --name $(PROJECT_NAME)-lint --rm -i -v "$(CURRENT_DIR):/$(PROJECT_NAME)" -e "GOPATH=/$(PROJECT_NAME)" -w /$(PROJECT_NAME) golangci/golangci-lint:v1.27-alpine golangci-lint run ./src/$(PROJECT_NAME)/... -E gofmt --skip-dirs=./src/$(PROJECT_NAME)/vendor --timeout=10m

.PHONY: up
