# Model Converter
GRPC-сервис конвертации моделей: `.glb -> .usdz`.

# Proto
Вообще, proto-файлы лежат в отдельном репо, но для примера, последняя версия выглядит так:
```protobuf
syntax = "proto3";

package modelconverter;

service modelconverter {
    rpc ConvertModel(ConvertModelRequest) returns (ConvertModelResponse); 
}

message ConvertModelRequest {
    string glb_file_source = 1;
}

message ConvertModelResponse {
    string usdz_file_source = 1;
}
```

## Env's
Для работы сервиса нужны переменные окружения, к-ые кладутся в корне, в .env-файл:

- `APP_PORT` - порт, на к-ом запускается сервис
- `LOG_LEVEL` - уровень логгирования, по-умолчанию - info
- `S3_URL` - S3-bucket
- `S3_REGION` - регион, в к-ом находится S3-bucket
- `S3_BUCKET_NAME` - название bucket'a S3
- `S3_ACCESS_KEY_ID` - ключ AWS
- `S3_SECRET_ACCESS_KEY` - секретный ключ AWS