module modelconverter

go 1.15

require (
	github.com/aws/aws-sdk-go v1.36.19
	github.com/caarlos0/env/v6 v6.4.0
	github.com/golang/protobuf v1.4.2
	github.com/google/uuid v1.1.3
	github.com/sirupsen/logrus v1.7.0
	google.golang.org/grpc v1.34.0
	google.golang.org/protobuf v1.25.0
)
