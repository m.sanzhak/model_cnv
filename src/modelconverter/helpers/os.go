package helpers

import (
	"fmt"
	"os"
)

func RemoveFiles(files ...string) error {
	for i := range files {
		if _, err := os.Stat(files[i]); os.IsNotExist(err) {
			return fmt.Errorf("file not exist %w", err)
		}
		err := os.Remove(files[i])
		if err != nil {
			return err
		}
	}

	return nil
}
