package main

import (
	"net"

	"modelconverter/handlers"
	proto "modelconverter/proto/modelconverter"
	"modelconverter/s3"

	"github.com/caarlos0/env/v6"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

type Config struct {
	AppPort           string `env:"APP_PORT,required"`
	LogLevel          string `env:"LOG_LEVEL" envDefault:"info"`
	S3URL             string `env:"S3_URL,required"`
	S3Region          string `env:"S3_REGION,required"`
	S3BucketName      string `env:"S3_BUCKET_NAME,required"`
	S3AccessKeyID     string `env:"S3_ACCESS_KEY_ID,required"`
	S3SecretAccessKey string `env:"S3_SECRET_ACCESS_KEY,required"`
}

func readConfig() (Config, error) {
	cfg := Config{}

	err := env.Parse(&cfg)
	if err != nil {
		return Config{}, err
	}

	log.SetReportCaller(true)
	log.SetFormatter(&log.JSONFormatter{})

	if level, err := log.ParseLevel(cfg.LogLevel); err == nil {
		log.SetLevel(level)
	} else {
		log.WithField("level", cfg.LogLevel).Warn("Invalid log level")
	}

	return cfg, nil
}

func main() {
	config, err := readConfig()
	if err != nil {
		log.WithError(err).Error("Read config error")
		return
	}

	s3Instance, err := s3.New(
		config.S3AccessKeyID,
		config.S3SecretAccessKey,
		config.S3BucketName,
		config.S3Region,
		config.S3URL,
	)
	if err != nil {
		log.WithError(err).Error("Create s3 error")
		return
	}

	handler := handlers.New(s3Instance)

	lis, err := net.Listen("tcp", ":"+config.AppPort)
	if err != nil {
		log.WithError(err).Error("Failed to listen")
		return
	}

	grpcServer := grpc.NewServer()
	proto.RegisterModelconverterServer(grpcServer, handler)

	if err := grpcServer.Serve(lis); err != nil {
		log.WithError(err).Error("Failed to serve")
		return
	}
}
