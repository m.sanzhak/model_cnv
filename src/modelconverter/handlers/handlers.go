package handlers

import (
	"context"
	"os"
	"os/exec"

	"modelconverter/helpers"
	proto "modelconverter/proto/modelconverter"

	log "github.com/sirupsen/logrus"
)

const (
	glbFile  = "model.glb"
	usdzFile = "model.usdz"
)

type S3 interface {
	UploadFile(file *os.File, filename string) (string, error)
	DownloadFile(url, glbFile string) error
}

type Handler struct {
	s3 S3
}

func New(s3 S3) *Handler {
	return &Handler{
		s3: s3,
	}
}

func (h *Handler) ConvertModel(ctx context.Context, req *proto.ConvertModelRequest) (*proto.ConvertModelResponse, error) {
	glbFileSource := req.GetGlbFileSource()
	log.WithField("glb model source", glbFileSource).Debug("New convert model request")
	err := h.s3.DownloadFile(glbFileSource, glbFile)
	if err != nil {
		_ = helpers.RemoveFiles(glbFile)
		log.WithError(err).Error("Download file error")
		return nil, err
	}

	cmd := exec.Command("usd_from_gltf", glbFile, usdzFile)
	err = cmd.Run()
	if err != nil {
		_ = helpers.RemoveFiles(glbFile, usdzFile)
		log.WithError(err).Error("Convert model error")
		return nil, err
	}

	usdzModel, err := os.Open(usdzFile)
	if err != nil {
		_ = helpers.RemoveFiles(glbFile, usdzFile)
		log.WithError(err).Error("Open converted file error")
		return nil, err
	}

	url, err := h.s3.UploadFile(usdzModel, "model.usdz")
	if err != nil {
		_ = helpers.RemoveFiles(glbFile, usdzFile)
		log.WithError(err).Error("Upload file error")
		return nil, err
	}

	if err := helpers.RemoveFiles(glbFile, usdzFile); err != nil {
		log.WithError(err).Error("Remove glbFile and usdzFile error")
		return nil, err
	}

	return &proto.ConvertModelResponse{
		UsdzFileSource: url,
	}, nil
}
