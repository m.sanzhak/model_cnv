package s3

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/google/uuid"
)

const (
	objectACL = "public-read"
)

type S3 struct {
	session    *session.Session
	bucketName string
	origin     string
}

func New(accessKeyID, secretAccessKeyID, bucket, region, origin string) (*S3, error) {
	sess, err := session.NewSession(
		&aws.Config{
			Region: aws.String(region),
			Credentials: credentials.NewStaticCredentials(
				accessKeyID,
				secretAccessKeyID,
				""),
		},
	)
	if err != nil {
		return nil, err
	}
	return &S3{
		session:    sess,
		bucketName: bucket,
		origin:     origin,
	}, nil
}

func (s *S3) UploadFile(file *os.File, filename string) (string, error) {
	uuidFileName := randomFileName(filename)
	uploader := s3manager.NewUploader(s.session)

	_, err := uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(s.bucketName),
		ACL:    aws.String(objectACL),
		Key:    aws.String(uuidFileName),
		Body:   file,
	})
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%s%s", s.origin, uuidFileName), nil
}

func (s *S3) DownloadFile(url, glbFile string) error {
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println("Error download file", err)
		return err
	}
	defer resp.Body.Close()

	out, err := os.Create(glbFile)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, resp.Body)

	return err
}

func randomFileName(filename string) string {
	sl := strings.Split(filename, ".")
	return fmt.Sprintf("%s.%s", generateUUID(), sl[len(sl)-1])
}

func generateUUID() string {
	return uuid.New().String()
}
